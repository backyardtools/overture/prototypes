# Backyard Tools: Overture
## Prototype scripts

This repository contains prototype/scratch scripts I've created while reversing the formats of `Guilty Gear 2: Overture`.

These scripts are rough and created to test the formats before proper implementation of the formats into a real library/tool can commence.

Eventually, they will become irrelevant when Backyard Tools: Overture and the accompanying GUI application are created.

Included in this repository:

### unpkm.py
#### UNRAVELLER AND WEAVER OF DATA
Extracts and repacks the ANGEL PKM container format.

As some PKM files do not contain filenames, the tool makes an attempt to automatically identify and name each file based on their magic number. Alternatively, community-sourced name mappings exist. The latest name mapping file will be shipped in this repository.

Some parts of the format have yet to be solved but this script has been successful with all the files I've tested, correctly unpacking them and producing binary-identical repacks.

Usage:
```bash
unpkm.py -h #Shows help

unpkm.py [-o <optional output path>] <path to PKM> #Unpacks a PKM file. If no output path is provided, it will be unpacked to [current directory]/[PKM name]

unpkm.py -o <output path> <path to folder> #Packs a PKM file. Output path is required. PKM will be written to output path
```

To extract the DAT.PAK file of Ys IV - Mask of the Sun - A New Theory, use this syntax:
```bash
unpkm.py [-o <optional output path>] -pki <path to PKI> <path to PKM>
```

Note that the PKM files contained within Ys' DAT.PAK are not currently supported.

### mftparse.py
#### MASTER OF TABLES
Dumps the game's MFT table files into JSON format. The dumped JSON is very raw and contains a lot of information that is unneccessary in terms of rebuilding the MFT. Also, the tool doesn't yet support rebuilding MFTs. Funny, that.

Always outputs to `data.json`.

Liable to be updated soon.

Usage:
```bash
mftparse.py <path to MFT> #Dumps the provided MFT to data.json
```

### bindecrypt.py
#### LINGUSITIC DEOBFUSCATOR
Decrypts the game's localisation `.bin` files (`data/outgame/w0.bin`, etc.).

Currently doesn't support re-encrypting.

Usage:

```bash
bindecrypt.py <path to bin>
```

### slkconv.py
#### TEDIOUS FORMAT CONVERTER
Highly-specific tool made solely to convert `data/settings/unitstat.slk` to a traditional CSV file.

Currently, it's a pain in the ass to properly view this file as it uses Shift-JIS encoding. The SLK format was not designed for non-ANSI character so naive tools such as LibreOffice will simply assume ANSI and not allow you to correct it, resulting in [mojibake](https://en.wikipedia.org/wiki/Mojibake).

This tool simply parses the bare minimum data from `unitstat.slk` and writes out a Shift-JIS CSV file which can be read by spreadsheet software.

Requires pandas. A pre-converted copy can be found in the [bits and bobs repository](https://gitlab.com/backyardtools/overture/bits_and_bobs).

Usage:
```bash
slkconv.py
```

Assumes that `unitstat.slk` is present in the current directory. Writes to `unitstat.csv`.