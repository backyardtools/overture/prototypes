import argparse
from pathlib import Path

def process(data, encrypting=False):
	uVar1 = 0
	decrypted_data = bytearray(len(data))
	
	for i in range(0, len(data), 2):
		ushort_val = int.from_bytes(data[i:i+2], byteorder='little')
		uVar1 = ushort_val ^ uVar1 ^ 58853
		decrypted_data[i:i+2] = uVar1.to_bytes(2, byteorder='little')
		if ( encrypting ):
			uVar1 = ushort_val

	return decrypted_data

parser: argparse.ArgumentParser = argparse.ArgumentParser()

parser.add_argument("i", help="Input file")
parser.add_argument("-o", help="Output file", required=False, default="decrypted.txt")

args = parser.parse_args()

encrypting = Path(args.i).suffix == '.txt'

mode = 'rb'
if ( encrypting ):
	mode = 'r'
with open(args.i, mode) as f:
	data = f.read()

if ( encrypting ):
	if ( args.o == "decrypted.txt"):
		args.o = "encrypted.bin"

	encoded = data.encode('utf-16be')
	decrypted_data = process(encoded, True)
else:
	decrypted_data = process(data)
	print(decrypted_data)

with open(args.o, 'wb') as f:
	f.write(decrypted_data)