import os
import sys
import struct
import argparse
import re
import json
from pathlib import Path

parser: argparse.ArgumentParser = argparse.ArgumentParser()

parser.add_argument("i", help="Input file or folder")

args = parser.parse_args()

mft_column_names = {
	0: "index",
	1: "index2",

	2: "unknown",
	3: "unknown",
	5: "unknown",
	6: "unknown",
	8: "unknown",
	9: "unknown",

	13: "Position X",
	14: "Position Y",
	15: "Position Z",

	16: "Rotation X",
	17: "Rotation Y",
	18: "Rotation Z",

	19: "Scale X",
	20: "Scale Y",
	21: "Scale Z",

	25: "unknown",

	104: "unknown",
	105: "unknown",
	106: "unknown",

	107: "Linked Ghost 1",
	108: "Linked Ghost 2",
	109: "Linked Ghost 3",
	110: "Linked Ghost 4",
	111: "Linked Ghost 5",
	112: "Linked Ghost 6",

	115: "unknown",
	116: "unknown",

	117: "master ghost flag",

	142: "LSO file",
	143: "BIN file",
}

mft_column_ids = {}

for k,v in mft_column_names.items():
	mft_column_ids[v] = k

def name_to_index(name: str) -> int:
	return mft_column_names[name]
def index_to_name(idx: int) -> str:
	if ( not idx in mft_column_ids ):
		return "unknown (unhandled)"
	return mft_column_ids[idx]

MFT_MAGIC = 0x4D465400

def readNTstring(f) -> str:
	chars = []
	while True:
		char = f.read(1)
		if ( char == b'\x00' or not char ):
			break
		chars.append(char)
	return b''.join(chars).decode('utf-8')

def parse_file(file: str) -> dict:
	mft = {}
	with open(file, 'rb') as f:
		header = f.read(8)
		magic, columns, rows = struct.unpack('Ihh', header)
		if ( magic != MFT_MAGIC ):
			print(f"Bad magic number, expected {MFT_MAGIC}, got {magic}")
			exit()

		mft["columns"] = columns
		mft["rows"] = rows

		mft["unkheader"] = int.from_bytes(f.read(4), byteorder="little")

		mft["columnids"] = [int.from_bytes(f.read(4), byteorder='little') for _ in range(columns)]

		#0 - int
		#1 - Float
		#2 - NT string
		#2 - Unsure. 4 bytes
		buf = f.read(4*columns)
		mft["data_types"] = struct.unpack("I"*columns, buf)

		mft["data_offsets"] = []
		for i in range(rows):
			buf = f.read(4*columns)
			mft["data_offsets"].append(struct.unpack("I"*columns, buf))

		origin = f.tell()

		mft["data"] = [[0 for _ in range(columns)] for _ in range(rows)]

		for i in range(rows):
			TEST = False
			offsets = mft["data_offsets"][i]

			for j in range(columns):
				f.seek(offsets[j], 1)
				match mft["data_types"][j]:
					case 0: #Int
						mft["data"][i][j] = int.from_bytes(f.read(4), byteorder="little")
					case 1: #Float
						buf = f.read(4)
						mft["data"][i][j] = struct.unpack('f', buf)[0]
					case 2: #NT string
						mft["data"][i][j] = readNTstring(f)
					case 3: #???
						print(i)
						print(j)
						print(offsets[j])
						mft["data"][i][j] = int.from_bytes(f.read(4), byteorder="little")
					case _:
						print(f"UNHANDLED DATA TYPE")
						print(f.tell())
						print(mft["data_types"][j])
						input()
				f.seek(origin, 0)
		
	return mft

if ( not os.path.isdir(args.i) ):
	mft = parse_file(args.i)
	with open("data.json", "w") as j:
		json.dump(mft, j, indent=4)
else:
	print("huh")
	#build_pkm(args.i)