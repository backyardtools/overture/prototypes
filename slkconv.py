import sys
import os
import pandas as pd

infile = 'unitstat.slk'
outfile = 'unitstat.csv'

content = None

with open(infile, 'r', encoding='shift_jis') as file:
	content = file.read()

lines = content.split('\n')


records = []

for line in lines:
	if not line:
		continue

	fields = line.split(';')

	record = {}
	record["type"] = fields[0]
	
	if ( record["type"] != 'D' ):
		
		for field in fields[1:]:
			if not field:
				continue;
			record[field[0]] = field[1:]
	
	print(record)
	records.append(record)

current_x = 1
current_y = 1

column_count = 3
row_count = 567

df = pd.DataFrame(index=range(row_count), columns=range(column_count))

def get(record, value: str):
	if value in record:
		return record[value]
	return None

for record in records:
	match record["type"]:
		case "E":
			break;
		case "F":
			x = get(record, 'X')
			y = get(record, 'Y')

			if x:
				current_x = int(x)
			if y:
				current_y = int(y)
		case "B":
			column_count = get(record, 'X')
			row_count = get(record, 'Y')
		case "C":
			x = get(record, 'X')
			y = get(record, 'Y')

			if x:
				current_x = int(x)
			if y:
				current_y = int(y)
			
			value = get(record, 'K').strip('"')
			#print(value)

			df.loc[current_y-1, current_x-1] = value

			#print(df.loc[current_y-1, current_x-1])

#print(df)

df.to_csv(outfile, encoding='shift_jis', index=False, header=False)