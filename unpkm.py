import os
import sys
import struct
import argparse
import re
import json
import dataclasses
from pathlib import Path

#https://stackoverflow.com/a/4623518
def tryint(s):
    try:
        return int(s)
    except:
        return s

def alphanum_key(s):
    """ Turn a string into a list of string and number chunks.
        "z23a" -> ["z", 23, "a"]
    """
    return [ tryint(c) for c in re.split('([0-9]+)', s) ]

def alphanum_key_dict(kv):
	return [ tryint(c) for c in re.split('([0-9]+)', kv[0]) ]

def sort_nicely(l):
    """ Sort the given list in the way that humans expect.
    """
    l.sort(key=alphanum_key)

def sort_dict_nicely(d : dict):
	return dict(sorted(d.items(), key=alphanum_key_dict))


parser: argparse.ArgumentParser = argparse.ArgumentParser()

parser.add_argument("i", help="Input file or folder")
parser.add_argument("-o", help="Out folder", required=False)
parser.add_argument("-t", help="(When building) PKM type.\n\t1 - Unknown\n\t2 - With filenames\n\t3 - Without filenames. Default: 3", required=False, default="3")
parser.add_argument("-vf", help="(When building) Enables DDSCAPS2_VOLUME on packed DDS files. Required by the game in some cases for unknown reasons.", required=False, action='store_true')
parser.add_argument("-ft", help="(When building) Specifies the filetable JSON to use for repacking. By default this is assumed to be in the directory you are packing. Filenames in the table should be relative to that directory.", required=False, default="")
parser.add_argument("-n", help="Specifies master name map JSON to use when unpacking", required=False, default="filemap.json")
parser.add_argument("-ne", help="Specifies name map entry to use when unpacking", required=False, default=None)
parser.add_argument("-pki", help="ANGEL PS2: Specify the PKI file", required=False, default=None)
args = parser.parse_args()

nameMap : dict = {}

PKM_MAGIC = 0x6678 #'xf'

def getAlignment(offs: int, amt: int):
	return amt - offs % amt

#No filenames in this bitch of a format but let's not make that the user's problem
#This should at least serve to make sorting through the files easier in these early days
def identify_file(filedata) -> str:
	magic = struct.unpack("I", filedata[:4])[0]

	if ( magic == 0x63637361 ):
		return "ASL" #Scripts. Could be ASB?
	elif ( magic == 542327876 ):
		return "DDS"
	elif ( magic == 0x46464952 ):
		return "WAV"
	elif magic == 1297239878:
		return "AFB" # Model files
	elif magic == 1296454656:
		return "MFT" #MFT files
	else:
		print(f"Unknown format: {magic}")
		return None

TYPE_FILENAMES = 2
TYPE_NONAMES = 3

def extract_file(file: str):
	pkiFile = None
	if ( args.pki != None ):
		pkiFile = open(args.pki, "rb")
		header = pkiFile.read(16)
		magic, endian, pkifilecount, unk1, type, unk3 = struct.unpack('hhIIhh', header)

		if magic != PKM_MAGIC:
			print(f"PKI: Bad magic number, expected {PKM_MAGIC}, got {magic}")
			exit()
			
		if endian == 1:
			print("Big endian PKI files aren't currently supported")
			exit()

		print(f"{args.pki} contains {pkifilecount} file info definitions")
		print(f"unk1: {unk1}")
		print(f"type: {type}")
		print(f"unk3: {unk3}")

	with open(file, "rb") as f:
		f.seek(0, 2)
		eof = f.tell()
		f.seek(0, 0)
		
		if ( args.pki == None ):
			header = f.read(16)
			magic, endian, filecount, unk1, type, unk3 = struct.unpack('hhIIhh', header)

			if magic != PKM_MAGIC:
				print(f"Bad magic number, expected {PKM_MAGIC}, got {magic}")
				exit()
			
			if endian == 1:
				print("Big endian PKM files aren't currently supported")
				exit()

			print(f"{file} contains {filecount} files")
			print(f"unk1: {unk1}")
			print(f"type: {type}")
			print(f"unk3: {unk3}")
			if (unk1 != 103 or ( type != TYPE_FILENAMES and type != TYPE_NONAMES ) or (unk3 != 16 and unk3 != 0)):
				print("Warning! This file has different values in the header. We don't know what these do yet. Might not extract or may not be able to repack.")
				print("You should report this.")
				input("Press enter to proceed anyway")
		
		nameMap = None

		if ( type == TYPE_NONAMES ):
			justFilename : str = Path(args.i).stem
			if ( args.ne != None ):
				justFilename = args.ne
			individualNamemap : str = f"filemap_{justFilename}.json"
			if ( not os.path.exists(args.n) ): # No master namemap
				if ( not os.path.exists(individualNamemap) ): # No split namemap either
					print(f"Master namemap {args.n} does not exist and neither does individual namemap {individualNamemap}.");
				else:
					print(f"Loading filenames from {individualNamemap}");
					
					with open(individualNamemap, "r") as nf:
						nameMap = json.load(nf)
			else:
				print(f"Loading master filename mapping file: {args.n}")
				with open(args.n, "r") as nf:
					masterNameMap = json.load(nf)
				print("Done")

				if ( not justFilename in masterNameMap ):
					print(f"No entry for {justFilename} found in master name map!\n")
				else:
					print(f"Loading name map for {justFilename}...")
					nameMap = masterNameMap[justFilename]
					if ( "alias" in nameMap ):
						alias = nameMap["alias"]
						if alias in masterNameMap:
							print(f"ALIAS {justFilename} -> {alias}")
							nameMap = masterNameMap[alias]
						else:
							print(f"Entry is aliased to none-existent entry {alias}")
							nameMap = None

			if nameMap == None:
				print("PKM will be extracted with numerical filenames.")

		fileinfos = []
		filenames = []

		if ( pkiFile != None ):
			for i in range(pkifilecount):
				nameBytes = pkiFile.read(256)
				name = nameBytes.decode('utf-8').rstrip('\x00')
				
				data = pkiFile.read(8)

				fileinfos.append(struct.unpack('II', data))
				filenames.append(name)

				print(f"PKI: file {name} is a {fileinfos[i][1]} byte real file at {fileinfos[i][0]}")
		else:
			for i in range(filecount):
				name = None
				if type == TYPE_FILENAMES:
					nameBytes = f.read(256);
					name = nameBytes.decode('utf-8').rstrip('\x00')
					
				#print(f"Reading fileinfo for file {i} of {filecount}")
				data = f.read(8)

				fileinfos.append(struct.unpack('II', data))
				if type == TYPE_FILENAMES:
					filenames.append(name)
				else:
					if nameMap != None and ( str(i) in nameMap ):
						filenames.append(nameMap[str(i)])
					else:
						filenames.append(None)

				if (fileinfos[i][1] == 0):
					print(f"file {i} is a dummy file pointer to {fileinfos[i][0]}")
				else:
					if ( name != None ):
						print(f"file {name} is a {fileinfos[i][1]} byte real file at {fileinfos[i][0]}")
					else:
						print(f"file {i} is a {fileinfos[i][1]} byte real file at {fileinfos[i][0]}")
		
		#filecount = filecount+pkifilecount

		if ( args.pki == None ):
			#Fucking hell
			f.seek(getAlignment(f.tell(), 64), 1)
		#input()

		dataStart = f.tell()
		print(f"file data starts at {dataStart}")

		filetable = {}
		fileData = []

		#Read file data
		idx = 0
		for info in fileinfos:
			offs, size = info[0], info[1]
			name = None
			if ( filenames[idx] != None ):
				name = filenames[idx]
			if size == 0:
				filetable[idx] = ""
				fileData.append(None)
				idx += 1
				continue
			
			f.seek(dataStart, 0)

			f.seek(offs, 1)

			if (name == None):
				print(f"reading file at {offs} of length {size}")
			else:
				print(f"reading {name} at {offs} of length {size}")

			fileData.append(f.read(size))
			idx += 1

		if ( args.o == None ):
			args.o = f"./{Path(args.i).stem}"

		if not os.path.exists(args.o):
			os.makedirs(args.o)
		
		curFile = 0

		for data in fileData:

			if data == None:
				#Dummy file, prolly
				#At least, I hope so
				curFile += 1
				continue
			elif ( filenames[curFile] != None):
				name = f"./{filenames[curFile]}"
			else:
				name = f"./{curFile}"
				fileType = identify_file(data)
				if ( fileType != None ):
					name = name + "." + fileType

			filetable[curFile] = name

			outPath = os.path.join(args.o, name)
			pathDir = os.path.dirname(outPath)
			if ( not os.path.isdir(pathDir) ):
				os.makedirs(pathDir)

			with open(outPath, "wb") as outF:
				print(f"Writing file {curFile}: {outPath}")
				outF.write(data)
			
			curFile += 1
		
		jsonOutPath = os.path.join(args.o, "./filetable.json")
		with open(jsonOutPath, "w") as outJ:
			json.dump(filetable, outJ, indent="\t", sort_keys=True)

def build_pkm(dir: str):
	if args.o == None:
		print("Building a PKM requires you to specify the output file!")
		exit()

	type = int(args.t)

	if (type != TYPE_NONAMES):
		print("Building non-type 2 PKMs is not supported yet.")
		exit()

	filetable = None
	with open(args.o, "wb") as f:

		filestemp = []

		if ( args.ft == "" ):
			filetableFile : str = os.path.join(dir, "filetable.json")
		else:
			filetableFile : str = args.ft
		
		if ( os.path.exists(filetableFile) ):
			with open(filetableFile, "r") as ft:
				filetable = json.load(ft)
		else:
			print(f"Error: No filetable found at {filetableFile}.")
			return

		if ( filetable != None ):
			filetable = sort_dict_nicely(filetable)
			

		header = struct.pack("IIIhh", PKM_MAGIC, len(filetable), 103, 3, 16) #Need to verify that these values are global for all PKMs. If not, what are they?

		f.write(header)

		#file infos
		curOffs = 0
		fileIdx = 0
		fileInfoStart = f.tell()
		fileInfosToWrite = []
		
		#Compile all our file info structs
		for idx,file in filetable.items():
			if ( file == "" ):
				#No file for this index - dummy file
				filelen = 0
			else:
				file = os.path.join(dir, file) #Always relative to filetable, so relative to dir we're packing
				filetable[idx] = file #Fixup in the dict itself for later down the line
				filelen = os.stat(file).st_size

			entry = struct.pack("II", curOffs, filelen)
			
			#print(f"data {curOffs} {filelen}")
				
			fileInfosToWrite.append(entry)
			
			curOffs += filelen
			align = getAlignment(curOffs, 16)
			if ( align != 16 ):
				curOffs += align
		endOfFileOffs = curOffs
				
		#Now run through them all and fill in offsets for the dummy file pointers
		#Dummy file pointers will always point to the next valid file
		nextRealFileOffset = 0
		fileIdx = 0
		finalDummy = False
		for info in fileInfosToWrite:
			curInfo = struct.unpack('II', info)
			if ( curInfo[1] == 0 ): #Dummy file
				#print(f"{fileIdx} is a dummy, offset will become {nextRealFileOffset}")
				fileInfosToWrite[fileIdx] = struct.pack('II', nextRealFileOffset, 0)
			elif not finalDummy:
				#print(f"{fileIdx} is real, recalculating offset of following dummy files")
				#Finished up our last batch of dummy files, find the next real file
				nextFile = fileIdx + 1
				while ( nextFile < len(fileInfosToWrite) ):
					nextInfo = struct.unpack('II', fileInfosToWrite[nextFile])
					offset = nextInfo[0]
					size = nextInfo[1]
					if ( size != 0 ): 
						print(f"File {nextFile} details: {offset} {size}")
						print(f"{nextFile} is our next actual file, at offset {offset}")
						nextRealFileOffset = offset
						print(f"New dummy offset: {nextRealFileOffset}")
						break
					nextFile += 1
				if ( nextFile >= len(fileInfosToWrite) ):
					finalDummy = True
					#print("ALL FOLLOWING DUMMIES POINT TO EOF")
					nextRealFileOffset = endOfFileOffs
					
			fileIdx += 1
			
		fileIdx = 0
		for info in fileInfosToWrite:
			f.write(info)
			fileIdx += 1

			#TODO: is this right?
			curOffs += filelen
			align = getAlignment(curOffs, 16)
			if ( align != 16 ):
				curOffs += align
		
		#Pad
		padding = b'\x00' * getAlignment(f.tell(), 64)
		f.write(padding)
		dataStart = f.tell()

		#Write file data
		for idx,file in filetable.items():
			if ( file == "" ):
				continue #It's a dummy, there's nothing to write.
			filename = file

			with open(filename, "rb") as ff:

				data = ff.read()
				
				if identify_file(data) == "DDS" and args.vf:
					print("File is DDS, hacking DDSCAPS2_VOLUME to be set")
					dataBytes = bytearray(data)
					dataBytes[114] = 0x20
					data = bytes(dataBytes)

				print(f"packing file {idx}: {file}")
				f.write(data)

				#TODO: again, is this correct?
				align = getAlignment(f.tell(), 16)
				if align != 16:
					padding = b'\x00' * align
					f.write(padding)

"""def parse_test(file: str):
	with open(file, "rb") as f:
		header = f.read(16)
		magic, endian, filecount, unk1, type, unk3 = struct.unpack('hhIIhh', header)

		if magic != PKM_MAGIC:
			print(f"Bad magic number, expected {PKM_MAGIC}, got {magic}")
			exit()
		
		if endian == 1:
			print(f"{file} is big endian")
			return

		if endian != 0 and endian != 1:
			print(f"{file} needs investigating")
			return
		
		print(f"{file} contains {filecount} files")
		print(f"unk1: {unk1}")
		print(f"type: {type}")
		print(f"unk3: {unk3}")
		if (unk1 != 103 or ( type != TYPE_FILENAMES and type != TYPE_NONAMES ) or unk3 != 16):
			print("Warning! This file has different values in the header. We don't know what these do yet. Might not extract or may not be able to repack.")
			print("You should report this.")
			input("Press enter to proceed anyway")

def check_pkms():
	for root, dirs, files in os.walk("D:\Program Files (x86)\Steam\steamapps\common\GG2\data"):
		for file in files:
			if (file.endswith(".PKM")):
				#print(os.path.join(root, file))
				parse_test(os.path.join(root, file))

check_pkms();"""

if ( not os.path.isdir(args.i) ):	
	extract_file(args.i)
else:
	build_pkm(args.i)
	
#print("Extraction complete!")